﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [ENTERPRISE_TYPES] (
    [CD_ENTERPRISE_TYPE] int NOT NULL IDENTITY,
    [NM_ENTERPRISE_TYPE] VARCHAR(50) NOT NULL,
    CONSTRAINT [PK_ENTERPRISE_TYPES] PRIMARY KEY ([CD_ENTERPRISE_TYPE])
);

GO

CREATE TABLE [INVESTORS] (
    [CD_INVESTOR] int NOT NULL IDENTITY,
    [NM_INVESTOR] VARCHAR(50) NOT NULL,
    [DS_EMAIL] VARCHAR(100) NOT NULL,
    [DS_PASSWORD] varchar(32) NOT NULL,
    [NM_CITY] VARCHAR(50) NOT NULL,
    [NM_COUNTRY] VARCHAR(50) NOT NULL,
    [VL_BALANCE] NUMERIC(15,2) NOT NULL DEFAULT 0.0,
    [URL_PHOTO] VARCHAR(200) NOT NULL,
    [VL_PORTFOLIO] NUMERIC(15,2) NOT NULL DEFAULT 0.0,
    [FL_FIRST_ACCESS] bit NOT NULL DEFAULT CAST(1 AS bit),
    [FL_SUPER_ANGEL] bit NOT NULL DEFAULT CAST(0 AS bit),
    CONSTRAINT [PK_INVESTORS] PRIMARY KEY ([CD_INVESTOR])
);

GO

CREATE TABLE [ENTERPRISES] (
    [CD_ENTERPRISE] int NOT NULL IDENTITY,
    [NM_ENTERPRISE] VARCHAR(50) NOT NULL,
    [DS_ENTERPRISE] VARCHAR(1000) NULL,
    [DS_EMAIL] VARCHAR(100) NULL,
    [URL_FACEBOOK] VARCHAR(200) NULL,
    [URL_TWITTER] VARCHAR(200) NULL,
    [URL_LINKEDIN] VARCHAR(200) NULL,
    [DS_PHONE] VARCHAR(14) NULL,
    [FL_OWN_ENTERPRISE] bit NOT NULL,
    [URL_PHOTO] VARCHAR(300) NULL,
    [VL_ENTERPRISE] decimal(18,2) NOT NULL,
    [QT_SHARES] int NOT NULL,
    [VL_SHARE_PRICE] decimal(18,2) NOT NULL,
    [QT_OWN_SHARES] int NOT NULL,
    [NM_CITY] VARCHAR(50) NOT NULL,
    [NM_COUNTRY] VARCHAR(50) NOT NULL,
    [CD_ENTERPRISE_TYPE] int NOT NULL,
    CONSTRAINT [PK_ENTERPRISES] PRIMARY KEY ([CD_ENTERPRISE]),
    CONSTRAINT [FK_ENTERPRISES_ENTERPRISE_TYPES_CD_ENTERPRISE_TYPE] FOREIGN KEY ([CD_ENTERPRISE_TYPE]) REFERENCES [ENTERPRISE_TYPES] ([CD_ENTERPRISE_TYPE]) ON DELETE NO ACTION
);

GO

CREATE TABLE [PORTFOLIOS] (
    [IdInvestor] int NOT NULL,
    [IdEnterprise] int NOT NULL,
    CONSTRAINT [PK_PORTFOLIOS] PRIMARY KEY ([IdEnterprise], [IdInvestor]),
    CONSTRAINT [FK_PORTFOLIOS_ENTERPRISES_IdEnterprise] FOREIGN KEY ([IdEnterprise]) REFERENCES [ENTERPRISES] ([CD_ENTERPRISE]) ON DELETE NO ACTION,
    CONSTRAINT [FK_PORTFOLIOS_INVESTORS_IdInvestor] FOREIGN KEY ([IdInvestor]) REFERENCES [INVESTORS] ([CD_INVESTOR]) ON DELETE NO ACTION
);

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'CD_ENTERPRISE_TYPE', N'NM_ENTERPRISE_TYPE') AND [object_id] = OBJECT_ID(N'[ENTERPRISE_TYPES]'))
    SET IDENTITY_INSERT [ENTERPRISE_TYPES] ON;
INSERT INTO [ENTERPRISE_TYPES] ([CD_ENTERPRISE_TYPE], [NM_ENTERPRISE_TYPE])
VALUES (1, 'Agro'),
(2, 'Aviation'),
(3, 'Biotech'),
(4, 'Eco'),
(5, 'Ecommerce'),
(6, 'Education'),
(7, 'Fashion'),
(8, 'Fintech'),
(9, 'Food'),
(10, 'Games');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'CD_ENTERPRISE_TYPE', N'NM_ENTERPRISE_TYPE') AND [object_id] = OBJECT_ID(N'[ENTERPRISE_TYPES]'))
    SET IDENTITY_INSERT [ENTERPRISE_TYPES] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'CD_INVESTOR', N'VL_BALANCE', N'NM_CITY', N'NM_COUNTRY', N'DS_EMAIL', N'FL_FIRST_ACCESS', N'NM_INVESTOR', N'DS_PASSWORD', N'URL_PHOTO', N'VL_PORTFOLIO') AND [object_id] = OBJECT_ID(N'[INVESTORS]'))
    SET IDENTITY_INSERT [INVESTORS] ON;
INSERT INTO [INVESTORS] ([CD_INVESTOR], [VL_BALANCE], [NM_CITY], [NM_COUNTRY], [DS_EMAIL], [FL_FIRST_ACCESS], [NM_INVESTOR], [DS_PASSWORD], [URL_PHOTO], [VL_PORTFOLIO])
VALUES (1, 350000.0, 'BH', 'Brasil', 'testeapple@ioasys.com.br', CAST(1 AS bit), 'Teste Apple', 'ed2b1f468c5f915f3f1cf75d7068baae', '/uploads/investor/photo/1/cropped4991818370070749122.jpg', 350000.0),
(2, 200000.0, 'SP', 'Brasil', 'testemicrosoft@ioasys.com.br', CAST(1 AS bit), 'Teste Microsoft', 'ed2b1f468c5f915f3f1cf75d7068baae', '/uploads/investor/photo/1/cropped4991818370070749122.jpg', 200000.0);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'CD_INVESTOR', N'VL_BALANCE', N'NM_CITY', N'NM_COUNTRY', N'DS_EMAIL', N'FL_FIRST_ACCESS', N'NM_INVESTOR', N'DS_PASSWORD', N'URL_PHOTO', N'VL_PORTFOLIO') AND [object_id] = OBJECT_ID(N'[INVESTORS]'))
    SET IDENTITY_INSERT [INVESTORS] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'CD_ENTERPRISE', N'CD_ENTERPRISE_TYPE', N'NM_CITY', N'NM_COUNTRY', N'DS_ENTERPRISE', N'DS_EMAIL', N'URL_FACEBOOK', N'URL_LINKEDIN', N'NM_ENTERPRISE', N'FL_OWN_ENTERPRISE', N'QT_OWN_SHARES', N'DS_PHONE', N'URL_PHOTO', N'VL_SHARE_PRICE', N'QT_SHARES', N'URL_TWITTER', N'VL_ENTERPRISE') AND [object_id] = OBJECT_ID(N'[ENTERPRISES]'))
    SET IDENTITY_INSERT [ENTERPRISES] ON;
INSERT INTO [ENTERPRISES] ([CD_ENTERPRISE], [CD_ENTERPRISE_TYPE], [NM_CITY], [NM_COUNTRY], [DS_ENTERPRISE], [DS_EMAIL], [URL_FACEBOOK], [URL_LINKEDIN], [NM_ENTERPRISE], [FL_OWN_ENTERPRISE], [QT_OWN_SHARES], [DS_PHONE], [URL_PHOTO], [VL_SHARE_PRICE], [QT_SHARES], [URL_TWITTER], [VL_ENTERPRISE])
VALUES (1, 1, 'Maule', 'Chile', 'Cold Killer was discovered by chance in the �90 s and developed by Mrs. In�s Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ', NULL, NULL, NULL, 'AQM S.A.', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(12, 2, 'Santiago', 'Chile', 'NexAtlas develops a "Google Maps for aviation" application for private and business aviation pilots. It consIderably facilitates access and handling of the most important flight planning and air navigation related information.', NULL, NULL, NULL, 'NexAtlas', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(7, 3, 'Santiago', 'Chile', 'The RapId  Antibiotic Sensitivty Kit  developed by Diagnochip allows  people having a urinary tract infection to receive the results of the proper antibiotic to use in only 8 hours versus the 3 to 4 days required by the traditional methodology.  Our kit has a big social impact as close the gap in the access to medical care for vulnerable people living in developing countries  and rural areas merging accuraCity of the results with cost - efficiency.', NULL, NULL, NULL, 'Diagnochip SpA', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(2, 4, 'Santiago', 'Chile', 'Capta Hydro is a clean energy technology startup that develops and commercialises hydropower technology for generation in artificial canals without the need of falls, that are economically competitive to other distributed generation alternatives and electric utility tariffs. Our BHAG is to install 1 GW of our technology  by 2027.', NULL, NULL, NULL, 'Capta Hydro', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(4, 4, 'ProvIdencia', 'Chile', 'The business of changing the world into a green one is not an easy task. eGreen believes that by making things easy for users, the growth and impact of an environmentally friendly society can be greatly accelerated. eGreen''s technology allows users to both calculate and offset their carbon footprint simply and free.  ', NULL, NULL, NULL, 'CO2fine spa (egreen.green)', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(11, 5, 'Santiago', 'Chile', 'We believe that the world would be a better place to live in, if more companies were purpose-driven companies. There is a global trend of purpose-driven companies, aiming to solve a social and/or environmental challenges through their business. There�s also a growing number of consumers declaring to be �conscious� and wanting to know what they are consuming and which are the companies behind the products and services they prefer. Finally, these consumers are using more and more e-commerce to buy their products and services. The problem is that purpose-driven companies and conscious consumers don�t have a common place to find each other. BirusMarket.com is the online platform that allows conscious consumers, not only to find and get to know these purpose-driven companies, but also to buy their products and services. BirusMarket.com business model consIders incomes from sales fee.', NULL, NULL, NULL, 'MercadoBirus.com', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(13, 5, 'Santiago', 'Chile', 'Impresee allows your e-commerce customers to find the products they want, whenever they want, thanks to its revolutionary search technology that uses Photos, sketches and text. Finding products with Impresee is easy and fun, whether it is a carpet seen at a friend''s house or a special lamp they found really attractive. The cloud based API offered by Impresee allows any retail business to use this advanced search technology in its on-line stores, along with a data analysis system for a better understanding of the behavior and preferences of its customers. ', '', '', '', 'Orand / Impresee', CAST(0 AS bit), 0, '', '/uploads/enterprise/Photo/34/americamg.png', 5000.0, 0, '', 0.0),
(10, 6, 'Santiago', 'Chile', 'HoliPlay Games is proud to present HoliMaths X, award winning family math games. A must have tool for families that understand how effective learning through playing can be.   Hi! I''m Matt from Holiplay Games!  HoliMaths X - a new family math strategy card game  We have an award winning game with 20+ outstanding reviews done by the most prominent Homeschoolers from the IHSNET network and other Parenting bloggers, communities and websites. It is the winner of the Academics'' Choice Awards 2016 Winner! Brain Toy category. 
                ehind due to all the technology that surrounds us.
                ', NULL, NULL, NULL, 'HoliPlay Games', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(3, 8, 'Santiago', 'Chile', 'Our product, Ceptinel Risk Manager, is a real-time business procecess monitoring system that, with the use of applied business rules algorithms, detects and notifies when potentially harmful or beneficial situations are detected.', NULL, NULL, NULL, 'Ceptinel', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(5, 8, 'Santiago', 'Chile', 'Coinaction provIdes worldwIde cheaper currency exchange. ', NULL, NULL, NULL, 'Coinaction', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(6, 8, 'Santiago', 'Chile', 'CryptoMarket is a Chilean Fintech product created by both an engineer and a lawyer.    It is based on trust and development on Blockchain technology; CryptoMarket permits a new way of sending and receiving inmediatamente Value, simple, trustworthy and safe,  integrating Chilean tradicional banking systems to the Blockchain. ', NULL, NULL, NULL, 'CryptoMkt', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(14, 8, 'Las Condes', 'Chile', 'PARADIGMA Ltda. has committed itself to deliver complete Solutions for agents and final consumers with the desire to promote the viability of the Financial Inclusion and building of assets among low-income indivIduals, small firms and their families on a large scale with Mobile devices, Computers, the Internet cloud, and the trust that brings the emergent technology Blockchain.', NULL, NULL, NULL, 'Paradigma Ltda', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(8, 9, 'Villa alemana', 'Chile', 'We are a R&D company , We are asociate with big manufacturer Company to test technology and domestic market acceptance . We are giving entrepreneurs services to acomplish goals in getting new products under quality certification and HACCP standards .We can give the scientific research to new food products under healthy and nutritional standards caring enviroment (sostain and recycling ), social and human rights and fairtrade standards . Our goal is having a Ideas trade tank giving them chances and yield to allow any people invest in them t support their success.', NULL, NULL, NULL, 'frutnutspa', CAST(0 AS bit), 0, NULL, NULL, 5000.0, 0, NULL, 0.0),
(9, 9, 'Las Condes', 'Chile', 'green equitable is a community (franchise) of  restaurants, stores and markets where regular  and conscious people can choose top healthy,  local, organic and fair trade products while  preserving the environment. ', '', '', '', 'Green Equitable', CAST(0 AS bit), 0, '', '/uploads/enterprise/Photo/22/diversas-imagens-fofas-50.jpg', 5000.0, 0, '', 0.0);
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'CD_ENTERPRISE', N'CD_ENTERPRISE_TYPE', N'NM_CITY', N'NM_COUNTRY', N'DS_ENTERPRISE', N'DS_EMAIL', N'URL_FACEBOOK', N'URL_LINKEDIN', N'NM_ENTERPRISE', N'FL_OWN_ENTERPRISE', N'QT_OWN_SHARES', N'DS_PHONE', N'URL_PHOTO', N'VL_SHARE_PRICE', N'QT_SHARES', N'URL_TWITTER', N'VL_ENTERPRISE') AND [object_id] = OBJECT_ID(N'[ENTERPRISES]'))
    SET IDENTITY_INSERT [ENTERPRISES] OFF;

GO

CREATE INDEX [IX_ENTERPRISES_CD_ENTERPRISE_TYPE] ON [ENTERPRISES] ([CD_ENTERPRISE_TYPE]);

GO

CREATE INDEX [IX_PORTFOLIOS_IdInvestor] ON [PORTFOLIOS] ([IdInvestor]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200723231528_InitialMigration', N'3.1.2');

GO

