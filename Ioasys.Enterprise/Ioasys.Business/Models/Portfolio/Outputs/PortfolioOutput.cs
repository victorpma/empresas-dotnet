﻿using Ioasys.Business.Models.Enterprise.Outputs;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ioasys.Business.Models
{
    public class PortfolioOutput
    {
        public PortfolioOutput()
        {
            Enterprises = new List<EnterpriseOutput>();
        }

        [JsonProperty("enterprises_number")]
        public long Count { get { return Enterprises.Count; } }

        [JsonProperty("enterprises")]
        public List<EnterpriseOutput> Enterprises { get; set; }
    }
}
