﻿using System.ComponentModel.DataAnnotations;

namespace Ioasys.Business.Models
{
    public class AuthInput
    {
        [Required(ErrorMessage = "The field {0} is required.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The field {0} is required.")]
        public string Password { get; set; }
    }
}
