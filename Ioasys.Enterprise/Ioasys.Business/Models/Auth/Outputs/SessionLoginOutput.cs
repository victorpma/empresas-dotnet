﻿using Ioasys.Business.Models.Enterprise.Outputs;
using Ioasys.Business.Models.Investor.Outputs;
using Newtonsoft.Json;
using System.Linq;
using entities = Ioasys.Data.Entities;

namespace Ioasys.Business.Models.Auth.Outputs
{
    public class SessionLoginOutput
    {
        public SessionLoginOutput(string token, entities.Investor investor)
        {
            Token = token;
            InvestorModel = new InvestorOutput()
            {
                Id = investor.Id,
                Name = investor.Name,
                Email = investor.Email,
                City = investor.City,
                Country = investor.Country,
                Balance = investor.Balance,
                Photo = investor.Photo,
                Portfolio = new PortfolioOutput()
                {
                    Enterprises = investor.Enterprises.Select(e => new EnterpriseOutput(e)).ToList()
                },
                PortfolioValue = investor.PortfolioValue,
                FirstAccess = investor.FirstAccess,
                SuperAngel = investor.SuperAngel
            };
        }

        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("investor")]
        public InvestorOutput InvestorModel { get; set; }

        [JsonProperty("success")]
        public bool Success { get => true; }
    }
}
