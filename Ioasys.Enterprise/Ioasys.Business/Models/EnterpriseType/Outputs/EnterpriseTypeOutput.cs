﻿using Newtonsoft.Json;

namespace Ioasys.Business.Models
{
    public class EnterpriseTypeOutput
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("enterprise_type_name")]
        public string Name { get; set; }
    }
}
