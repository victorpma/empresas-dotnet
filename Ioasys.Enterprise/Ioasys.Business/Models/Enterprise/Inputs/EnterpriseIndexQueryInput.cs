﻿using Microsoft.AspNetCore.Mvc;

namespace Ioasys.Business.Models.Enterprise.Outputs
{
    public class EnterpriseIndexQueryInput
    {
        [FromQuery(Name = "name")]
        public string Name { get; set; }

        [FromQuery(Name = "enterprise_types")]
        public int? TypeId { get; set; }
    }
}
