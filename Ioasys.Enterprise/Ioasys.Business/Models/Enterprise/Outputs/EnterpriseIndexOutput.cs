﻿using Ioasys.Business.Models.Enterprise.Outputs;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ioasys.Business.Models
{
    public class EnterpriseIndexOutput
    {
        public EnterpriseIndexOutput(List<EnterpriseOutput> enterpriseModels)
        {
            Enterprises = enterpriseModels;
        }

        [JsonProperty("enterprises")]
        public List<EnterpriseOutput> Enterprises { get; set; }

        [JsonProperty("success")]
        public bool Success { get => true; }
    }
}
