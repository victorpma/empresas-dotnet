﻿using Newtonsoft.Json;
using entities = Ioasys.Data.Entities;

namespace Ioasys.Business.Models.Enterprise.Outputs
{
    public class EnterpriseOutput
    {
        public EnterpriseOutput() { } //MAPPER CONSTRUCTOR

        public EnterpriseOutput(entities.Enterprise enterprise)
        {
            Id = enterprise.Id;
            Name = enterprise.Name;
            Facebook = enterprise.Facebook;
            Linkedin = enterprise.Linkedin;
            Phone = enterprise.Phone;
            OwnEnterprise = enterprise.OwnEnterprise;
            Photo = enterprise.Photo;
            Name = enterprise.Name;
            Description = enterprise.Description;
            City = enterprise.City;
            Country = enterprise.Country;
            Value = enterprise.Value;
            SharePrice = enterprise.SharePrice;
            EnterpriseType = new EnterpriseTypeOutput()
            {
                Id = enterprise.EnterpriseType.Id,
                Name = enterprise.EnterpriseType.Name
            };
        }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("email_enterprise")]
        public string Email { get; set; }

        [JsonProperty("facebook")]
        public string Facebook { get; set; }

        [JsonProperty("twitter")]
        public string Twitter { get; set; }

        [JsonProperty("linkedin")]
        public string Linkedin { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("own_enterprise")]
        public bool OwnEnterprise { get; set; }

        [JsonProperty("photo")]
        public string Photo { get; set; }

        [JsonProperty("enterprise_name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("value")]
        public decimal Value { get; set; }

        [JsonProperty("share_price")]
        public decimal SharePrice { get; set; }

        [JsonProperty("enterprise_type")]
        public EnterpriseTypeOutput EnterpriseType { get; set; }

    }
}
