﻿using Newtonsoft.Json;

namespace Ioasys.Business.Models.Enterprise.Outputs
{
    public class EnterpriseShowOutput
    {
        public EnterpriseShowOutput(EnterpriseOutput enterpriseOutput)
        {
            EnterpriseOutput = enterpriseOutput;
        }

        [JsonProperty("enterprise")]
        public EnterpriseOutput EnterpriseOutput { get; set; }

        [JsonProperty("success")]
        public bool Success { get => true; }
    }
}
