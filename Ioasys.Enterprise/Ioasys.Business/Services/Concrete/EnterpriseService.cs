﻿using AutoMapper;
using Ioasys.Business.Models;
using Ioasys.Business.Models.Enterprise.Outputs;
using Ioasys.Business.Services.Abstract;
using Ioasys.Data.Repository.Abstract;
using System.Collections.Generic;
using System.Linq;
using entities = Ioasys.Data.Entities;

namespace Ioasys.Business.Services.Concrete
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly IEnterpriseRepository _enterpriseRepository;
        private readonly IMapper _mapper;

        public EnterpriseService(IEnterpriseRepository enterpriseRepository,
                                 IMapper mapper)
        {
            _enterpriseRepository = enterpriseRepository;
            _mapper = mapper;
        }

        public EnterpriseShowOutput GetEnterprise(int id)
        {
            var enterprise = _enterpriseRepository.GetById(id, include: $"{nameof(entities.Enterprise.EnterpriseType)}");

            var enterpriseShowOutput = new EnterpriseShowOutput(_mapper.Map<EnterpriseOutput>(enterprise));

            return enterpriseShowOutput;
        }

        public EnterpriseIndexOutput GetEnterprises(EnterpriseIndexQueryInput enterpriseIndexQueryInput)
        {
            var enterprises = _enterpriseRepository.GetAll(include: $"{nameof(entities.Enterprise.EnterpriseType)}");

            enterprises = enterprises.Where(e => e.Name == enterpriseIndexQueryInput.Name || enterpriseIndexQueryInput.Name == null);
            enterprises = enterprises.Where(e => e.EnterpriseType.Id == enterpriseIndexQueryInput.TypeId || enterpriseIndexQueryInput.TypeId == null);

            var enterprisesModel = _mapper.Map<List<EnterpriseOutput>>(enterprises);

            return new EnterpriseIndexOutput(enterprisesModel);
        }
    }
}
