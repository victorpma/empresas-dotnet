﻿using Ioasys.Business.Models;
using Ioasys.Business.Models.Auth.Outputs;
using Ioasys.Business.Services.Abstract;
using Ioasys.Data.Entities;
using Ioasys.Data.Repository.Abstract;
using Ioasys.Util;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;

namespace Ioasys.Business.Services.Concrete
{
    public class AuthService : IAuthService
    {
        private readonly IInvestorRepository _investorRepository;
        private readonly IPortfolioRepository _portfolioRepository;
        private readonly IConfiguration _configuration;

        public AuthService(IInvestorRepository investorRepository,
                           IPortfolioRepository portfolioRepository,
                           IConfiguration configuration)
        {
            _investorRepository = investorRepository;
            _portfolioRepository = portfolioRepository;
            _configuration = configuration;
        }

        public SessionLoginOutput SignIn(AuthInput postAuthModel)
        {
            var investor = _investorRepository.GetByEmail(postAuthModel.Email, $"{nameof(Investor.Portfolios)}");

            if (investor == null || !CriptographyMD5.VerifyMd5Hash(postAuthModel.Password, investor.Password))
                throw new ApiException(HttpStatusCode.Unauthorized, "Invalid login credentials. Please try again.");

            var portfolios = _portfolioRepository.GeyByInvestor(investor.Id,
                $"{nameof(Portfolio.Enterprise)}.{nameof(Portfolio.Enterprise.EnterpriseType)}");

            investor.Enterprises = portfolios.Select(p => p.Enterprise).ToList();

            return GetSessionLogin(investor);
        }

        private SessionLoginOutput GetSessionLogin(Investor investor)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("Secret"));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
                    new Claim("Id", investor.Id.ToString()),
                    new Claim("Name", investor.Name),
                    new Claim("Email", investor.Email)
                }),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));

            var sessaoLogin = new SessionLoginOutput(token, investor);

            return sessaoLogin;
        }
    }
}
