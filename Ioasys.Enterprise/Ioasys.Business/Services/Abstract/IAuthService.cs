﻿using Ioasys.Business.Models;
using Ioasys.Business.Models.Auth.Outputs;

namespace Ioasys.Business.Services.Abstract
{
    public interface IAuthService
    {
        SessionLoginOutput SignIn(AuthInput postAuthModel);
    }
}
