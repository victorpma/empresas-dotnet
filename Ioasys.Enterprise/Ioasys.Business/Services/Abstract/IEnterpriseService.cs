﻿using Ioasys.Business.Models;
using Ioasys.Business.Models.Enterprise.Outputs;

namespace Ioasys.Business.Services.Abstract
{
    public interface IEnterpriseService
    {
        EnterpriseIndexOutput GetEnterprises(EnterpriseIndexQueryInput enterpriseIndexQueryInput);
        EnterpriseShowOutput GetEnterprise(int id);
    }
}
