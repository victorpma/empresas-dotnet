﻿using Ioasys.Business.Models;
using Ioasys.Business.Models.Auth.Outputs;
using Ioasys.Business.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ioasys.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/users/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
            => _authService = authService;

        [AllowAnonymous]
        [HttpPost("sign_in")]
        [ProducesResponseType(typeof(SessionLoginOutput), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(void), StatusCodes.Status401Unauthorized)]
        public IActionResult SignIn([FromBody] AuthInput postAuthModel)
            => Ok(_authService.SignIn(postAuthModel));

    }
}
