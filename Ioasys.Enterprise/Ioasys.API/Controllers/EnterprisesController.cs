﻿using Ioasys.Business.Models;
using Ioasys.Business.Models.Enterprise.Outputs;
using Ioasys.Business.Services.Abstract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ioasys.API.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseService _enterpriseService;

        public EnterprisesController(IEnterpriseService enterpriseService)
            => _enterpriseService = enterpriseService;

        [Authorize]
        [HttpGet]
        [ProducesResponseType(typeof(EnterpriseIndexOutput), StatusCodes.Status200OK)]
        public IActionResult Enterprises([FromQuery] EnterpriseIndexQueryInput enterpriseIndexQueryInput)
            => Ok(_enterpriseService.GetEnterprises(enterpriseIndexQueryInput));

        [Authorize]
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(EnterpriseShowOutput), StatusCodes.Status200OK)]
        public IActionResult Enterprise(int id)
           => Ok(_enterpriseService.GetEnterprise(id));
    }
}
