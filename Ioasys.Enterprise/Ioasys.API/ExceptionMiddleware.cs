﻿using Ioasys.API.Extensions;
using Ioasys.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Ioasys.API
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next,
                                   ILoggerFactory logger)
        {
            _next = next;
            _logger = logger.CreateLogger("Global Errors");
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ApiException ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                await HandleExceptionAsync(httpContext);
            }
        }

        public Task HandleExceptionAsync(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = context.Response.StatusCode,
                Message = "Opss! Aconteceu algo de errado =(."
            }.ToString());
        }

        public Task HandleExceptionAsync(HttpContext context, ApiException apiException)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)apiException.StatusCode;

            return context.Response.WriteAsync(new ErrorDetails()
            {
                StatusCode = (int)apiException.StatusCode,
                Message = apiException.Message
            }.ToString());
        }

    }
}
