﻿using AutoMapper;
using Ioasys.Business.Models;
using Ioasys.Business.Models.Enterprise.Outputs;
using Ioasys.Data.Entities;

namespace Ioasys.API.Configuration
{
    public class AutomapperConfig : Profile
    {
        public AutomapperConfig()
        {
            //GET
            CreateMap<Enterprise, EnterpriseOutput>().ReverseMap();
            CreateMap<EnterpriseType, EnterpriseTypeOutput>().ReverseMap();
          
        }
    }
}
