﻿using Ioasys.Business.Services.Abstract;
using Ioasys.Business.Services.Concrete;
using Ioasys.Data.Context;
using Ioasys.Data.Repository.Abstract;
using Ioasys.Data.Repository.Concrete;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;
using static Ioasys.API.Configuration.SwaggerConfig;

namespace Ioasys.API.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            //CONTEXT
            services.AddScoped<EnterpriseContext>();

            //REPOSITORIES
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IInvestorRepository, InvestorRepository>();
            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();
            services.AddScoped<IPortfolioRepository, PortfolioRepository>();

            //SERVICES
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IEnterpriseService, EnterpriseService>();

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            return services;
        }
    }
}
