﻿using Ioasys.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.IO;

namespace Ioasys.Data.Mappings
{
    public class EnterpriseMapping : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.HasKey(e => e.Id);

            builder.Property(e => e.Id)
                   .ValueGeneratedOnAdd()
                   .IsRequired()
                   .HasColumnName("CD_ENTERPRISE");

            builder.Property(e => e.Name)
                   .IsRequired()
                   .HasColumnType("VARCHAR(50)")
                   .HasColumnName("NM_ENTERPRISE");

            builder.Property(e => e.Description)
                   .HasColumnType("VARCHAR(1000)")
                   .HasColumnName("DS_ENTERPRISE");

            builder.Property(e => e.Email)
                   .HasColumnType("VARCHAR(100)")
                   .HasColumnName("DS_EMAIL");

            builder.Property(e => e.Facebook)
                   .HasColumnType("VARCHAR(200)")
                   .HasColumnName("URL_FACEBOOK");

            builder.Property(e => e.Twitter)
                   .HasColumnType("VARCHAR(200)")
                   .HasColumnName("URL_TWITTER");

            builder.Property(e => e.Linkedin)
                   .HasColumnType("VARCHAR(200)")
                   .HasColumnName("URL_LINKEDIN");

            builder.Property(e => e.Phone)
                   .HasColumnType("VARCHAR(14)")
                   .HasColumnName("DS_PHONE");

            builder.Property(e => e.OwnEnterprise)
                   .IsRequired()
                   .HasColumnName("FL_OWN_ENTERPRISE");

            builder.Property(e => e.Photo)
                   .HasColumnType("VARCHAR(300)")
                   .HasColumnName("URL_PHOTO");

            builder.Property(e => e.Value)
                   .IsRequired()
                   .HasColumnName("VL_ENTERPRISE");

            builder.Property(e => e.Shares)
                   .IsRequired()
                   .HasColumnName("QT_SHARES");

            builder.Property(e => e.SharePrice)
                   .IsRequired()
                   .HasColumnName("VL_SHARE_PRICE");

            builder.Property(e => e.OwnShares)
                   .IsRequired()
                   .HasColumnName("QT_OWN_SHARES");

            builder.Property(e => e.City)
                   .IsRequired()
                   .HasColumnType("VARCHAR(50)")
                   .HasColumnName("NM_CITY");

            builder.Property(e => e.Country)
                  .IsRequired()
                  .HasColumnType("VARCHAR(50)")
                  .HasColumnName("NM_COUNTRY");

            builder.HasOne<EnterpriseType>(e => e.EnterpriseType)
                   .WithMany(t => t.Enterprises)
                   .HasForeignKey("CD_ENTERPRISE_TYPE")
                   .IsRequired()
                   .OnDelete(DeleteBehavior.Restrict);

            builder.Ignore(e => e.Investors);

            builder.ToTable("ENTERPRISES");

            builder.HasData(ReadJsonData());

        }

        private Enterprise[] ReadJsonData()
        {
            using (StreamReader reader = new StreamReader("..\\Ioasys.Data\\JsonData\\Enterprise.json"))
            {
                return JsonConvert.DeserializeObject<Enterprise[]>(reader.ReadToEnd());
            }
        }
    }
}
