﻿using Ioasys.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.IO;

namespace Ioasys.Data.Mappings
{
    public class EnterpriseTypeMapping : IEntityTypeConfiguration<EnterpriseType>
    {
        public void Configure(EntityTypeBuilder<EnterpriseType> builder)
        {
            builder.HasKey(t => t.Id);

            builder.Property(t => t.Id)
                   .ValueGeneratedOnAdd()
                   .IsRequired()
                   .HasColumnName("CD_ENTERPRISE_TYPE");

            builder.Property(t => t.Name)
                   .IsRequired()
                   .HasColumnType("VARCHAR(50)")
                   .HasColumnName("NM_ENTERPRISE_TYPE");

            builder.ToTable("ENTERPRISE_TYPES");

            builder.HasData(ReadJsonData());
        }

        private EnterpriseType[] ReadJsonData()
        {
            using (StreamReader reader = new StreamReader("..\\Ioasys.Data\\JsonData\\EnterpriseType.json"))
            {
                return JsonConvert.DeserializeObject<EnterpriseType[]>(reader.ReadToEnd());
            }
        }
    }
}
