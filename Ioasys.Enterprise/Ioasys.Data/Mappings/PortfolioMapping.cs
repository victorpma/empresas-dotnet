﻿using Ioasys.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Data.Mappings
{
    public class PortfolioMapping : IEntityTypeConfiguration<Portfolio>
    {
        public void Configure(EntityTypeBuilder<Portfolio> builder)
        {
            builder.HasKey(p => new
            {
                p.IdEnterprise,
                p.IdInvestor
            });

            builder.HasOne<Investor>(p => p.Investor)
                   .WithMany(i => i.Portfolios)
                   .HasForeignKey(p => p.IdInvestor)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne<Enterprise>(p => p.Enterprise)
                   .WithMany(e => e.Portfolios)
                   .HasForeignKey(p => p.IdEnterprise)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.ToTable("PORTFOLIOS");

            builder.HasData(new Portfolio()
            {
                IdInvestor = 1,
                IdEnterprise = 1
            },
            new Portfolio()
            {
                IdInvestor = 1,
                IdEnterprise = 2
            },
             new Portfolio()
             {
                 IdInvestor = 1,
                 IdEnterprise = 4
             });
        }
    }
}
