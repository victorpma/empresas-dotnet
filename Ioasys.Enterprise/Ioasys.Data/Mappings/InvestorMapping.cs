﻿using Ioasys.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.IO;

namespace Ioasys.Data.Mappings
{
    public class InvestorMapping : IEntityTypeConfiguration<Investor>
    {
        public void Configure(EntityTypeBuilder<Investor> builder)
        {
            builder.HasKey(i => i.Id);

            builder.Property(i => i.Id)
                   .ValueGeneratedOnAdd()
                   .IsRequired()
                   .HasColumnName("CD_INVESTOR");

            builder.Property(i => i.Name)
                   .IsRequired()
                   .HasColumnType("VARCHAR(50)")
                   .HasColumnName("NM_INVESTOR");

            builder.Property(i => i.Email)
                   .IsRequired()
                   .HasColumnType("VARCHAR(100)")
                   .HasColumnName("DS_EMAIL");

            builder.Property(u => u.Password)
                  .IsRequired()
                  .HasColumnType("varchar(32)")
                  .HasColumnName("DS_PASSWORD");

            builder.Property(i => i.City)
                   .IsRequired()
                   .HasColumnType("VARCHAR(50)")
                   .HasColumnName("NM_CITY");

            builder.Property(i => i.Country)
                   .IsRequired()
                   .HasColumnType("VARCHAR(50)")
                   .HasColumnName("NM_COUNTRY");

            builder.Property(i => i.Balance)
                   .IsRequired()
                   .HasDefaultValue(0)
                   .HasColumnType("NUMERIC(15,2)")
                   .HasColumnName("VL_BALANCE");

            builder.Property(i => i.Photo)
                   .IsRequired()
                   .HasColumnType("VARCHAR(200)")
                   .HasColumnName("URL_PHOTO");

            builder.Property(i => i.PortfolioValue)
                   .IsRequired()
                   .HasDefaultValue(0)
                   .HasColumnType("NUMERIC(15,2)")
                   .HasColumnName("VL_PORTFOLIO");

            builder.Property(i => i.FirstAccess)
                   .IsRequired()
                   .HasDefaultValue(true)
                   .HasColumnName("FL_FIRST_ACCESS");

            builder.Property(i => i.SuperAngel)
                   .IsRequired()
                   .HasDefaultValue(false)
                   .HasColumnName("FL_SUPER_ANGEL");

            builder.Ignore(i => i.Enterprises);

            //builder.HasData(new Investor()
            //{
            //    Id = 1,
            //    Name = "Teste Apple",
            //    Email = "testeapple@ioasys.com.br",
            //    Password = "ed2b1f468c5f915f3f1cf75d7068baae", //PASSWORD: 12341234 EM MD5
            //    City = "BH",
            //    Country = "Brasil",
            //    Balance = 350000,
            //    Photo = "/uploads/investor/photo/1/cropped4991818370070749122.jpg",
            //    PortfolioValue = 350000,
            //    FirstAccess = true,
            //    SuperAngel = false
            //});

            builder.HasData(ReadJsonData());

            builder.ToTable("INVESTORS");
        }

        private Investor[] ReadJsonData()
        {
            using (StreamReader reader = new StreamReader("..\\Ioasys.Data\\JsonData\\Investor.json"))
            {
                return JsonConvert.DeserializeObject<Investor[]>(reader.ReadToEnd());
            }
        }
    }
}
