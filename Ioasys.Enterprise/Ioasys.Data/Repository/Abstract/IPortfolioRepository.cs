﻿using Ioasys.Data.Entities;
using System.Linq;

namespace Ioasys.Data.Repository.Abstract
{
    public interface IPortfolioRepository
    {
        IQueryable<Portfolio> GetAll(string include = "");
        IQueryable<Portfolio> GeyByInvestor(int idInvestor, string include = "");
    }
}
