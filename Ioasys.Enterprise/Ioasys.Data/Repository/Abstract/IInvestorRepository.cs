﻿using Ioasys.Data.Entities;
using System;

namespace Ioasys.Data.Repository.Abstract
{
    public interface IInvestorRepository : IDisposable
    {
        Investor GetByEmail(string email, string include = "");
    }
}
