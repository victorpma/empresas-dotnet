﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Ioasys.Data.Repository.Abstract
{
    public interface IBaseRepository<TEntity> : IDisposable where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAll(string include = "");
        IQueryable<TEntity> Search(Expression<Func<TEntity, bool>> predicate, string include = "");
        TEntity GetById(int id);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Remove(int id);
        int SaveChanges();
    }
}
