﻿using Ioasys.Data.Entities;
using System.Linq;

namespace Ioasys.Data.Repository.Abstract
{
    public interface IEnterpriseRepository
    {
        IQueryable<Enterprise> GetAll(string include = "");
        Enterprise GetById(int id, string include = "");
    }
}
