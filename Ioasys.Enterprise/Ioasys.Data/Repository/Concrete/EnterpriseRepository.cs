﻿using Ioasys.Data.Entities;
using Ioasys.Data.Repository.Abstract;
using System.Linq;

namespace Ioasys.Data.Repository.Concrete
{
    public class EnterpriseRepository : IEnterpriseRepository
    {
        public IBaseRepository<Enterprise> _baseRepository;

        public EnterpriseRepository(IBaseRepository<Enterprise> baseRepository)
            => _baseRepository = baseRepository;

        public IQueryable<Enterprise> GetAll(string include)
            => _baseRepository.GetAll(include);

        public Enterprise GetById(int id, string include)
            => _baseRepository.Search(e => e.Id == id, include).FirstOrDefault();
    }
}
