﻿using Ioasys.Data.Entities;
using Ioasys.Data.Repository.Abstract;
using System.Linq;

namespace Ioasys.Data.Repository.Concrete
{
    public class PortfolioRepository : IPortfolioRepository
    {
        private readonly IBaseRepository<Portfolio> _baseRepository;

        public PortfolioRepository(IBaseRepository<Portfolio> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public IQueryable<Portfolio> GetAll(string include = "")
            => _baseRepository.GetAll(include);

        public IQueryable<Portfolio> GeyByInvestor(int idInvestor, string include = "")
            => _baseRepository.Search(p => p.Investor.Id == idInvestor, include);
    }
}
