﻿using Ioasys.Data.Context;
using Ioasys.Data.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Ioasys.Data.Repository.Concrete
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected readonly EnterpriseContext _enterpriseContext;
        protected readonly DbSet<TEntity> _dbSet;

        public BaseRepository(EnterpriseContext enterpriseContext)
        {
            _enterpriseContext = enterpriseContext;
            _dbSet = _enterpriseContext.Set<TEntity>();
        }

        public virtual IQueryable<TEntity> GetAll() => _dbSet.AsNoTracking();

        public virtual IQueryable<TEntity> GetAll(string include)
            => _dbSet.AsNoTracking().Include(include);

        public IQueryable<TEntity> Search(Expression<Func<TEntity, bool>> predicate, string include = "")
            => _dbSet.AsNoTracking().Where(predicate).Include(include);

        public virtual TEntity GetById(int id) => _dbSet.Find(id);

        public virtual void Insert(TEntity entity)
        {
            _dbSet.Add(entity);
            SaveChanges();
        }

        public virtual void Update(TEntity entity)
        {
            _dbSet.Update(entity);
            SaveChanges();
        }

        public virtual void Remove(int id)
        {
            _dbSet.Remove(_dbSet.Find(id));
            SaveChanges();
        }

        public int SaveChanges() => _enterpriseContext.SaveChanges();


        public void Dispose()
            => _enterpriseContext?.Dispose();
    }
}
