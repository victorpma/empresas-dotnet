﻿using Ioasys.Data.Entities;
using Ioasys.Data.Repository.Abstract;
using System.Linq;

namespace Ioasys.Data.Repository.Concrete
{
    public class InvestorRepository : IInvestorRepository
    {
        private readonly IBaseRepository<Investor> _baseRepository;

        public InvestorRepository(IBaseRepository<Investor> baseRepository)
        {
            _baseRepository = baseRepository;
        }

        public Investor GetByEmail(string email, string include = "")
            => _baseRepository.Search(i => i.Email.ToLower() == email.ToLower(), include).FirstOrDefault();

        public void Dispose()
        {
            _baseRepository.Dispose();
        }
    }
}
