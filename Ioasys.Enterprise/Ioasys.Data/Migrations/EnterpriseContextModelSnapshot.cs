﻿// <auto-generated />
using Ioasys.Data.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Ioasys.Data.Migrations
{
    [DbContext(typeof(EnterpriseContext))]
    partial class EnterpriseContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Ioasys.Data.Entities.Enterprise", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CD_ENTERPRISE")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CD_ENTERPRISE_TYPE")
                        .HasColumnType("int");

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnName("NM_CITY")
                        .HasColumnType("VARCHAR(50)");

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasColumnName("NM_COUNTRY")
                        .HasColumnType("VARCHAR(50)");

                    b.Property<string>("Description")
                        .HasColumnName("DS_ENTERPRISE")
                        .HasColumnType("VARCHAR(1000)");

                    b.Property<string>("Email")
                        .HasColumnName("DS_EMAIL")
                        .HasColumnType("VARCHAR(100)");

                    b.Property<string>("Facebook")
                        .HasColumnName("URL_FACEBOOK")
                        .HasColumnType("VARCHAR(200)");

                    b.Property<string>("Linkedin")
                        .HasColumnName("URL_LINKEDIN")
                        .HasColumnType("VARCHAR(200)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("NM_ENTERPRISE")
                        .HasColumnType("VARCHAR(50)");

                    b.Property<bool>("OwnEnterprise")
                        .HasColumnName("FL_OWN_ENTERPRISE")
                        .HasColumnType("bit");

                    b.Property<int>("OwnShares")
                        .HasColumnName("QT_OWN_SHARES")
                        .HasColumnType("int");

                    b.Property<string>("Phone")
                        .HasColumnName("DS_PHONE")
                        .HasColumnType("VARCHAR(14)");

                    b.Property<string>("Photo")
                        .HasColumnName("URL_PHOTO")
                        .HasColumnType("VARCHAR(300)");

                    b.Property<decimal>("SharePrice")
                        .HasColumnName("VL_SHARE_PRICE")
                        .HasColumnType("decimal(18,2)");

                    b.Property<int>("Shares")
                        .HasColumnName("QT_SHARES")
                        .HasColumnType("int");

                    b.Property<string>("Twitter")
                        .HasColumnName("URL_TWITTER")
                        .HasColumnType("VARCHAR(200)");

                    b.Property<decimal>("Value")
                        .HasColumnName("VL_ENTERPRISE")
                        .HasColumnType("decimal(18,2)");

                    b.HasKey("Id");

                    b.HasIndex("CD_ENTERPRISE_TYPE");

                    b.ToTable("ENTERPRISES");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CD_ENTERPRISE_TYPE = 1,
                            City = "Maule",
                            Country = "Chile",
                            Description = "Cold Killer was discovered by chance in the �90 s and developed by Mrs. In�s Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ",
                            Name = "AQM S.A.",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 2,
                            CD_ENTERPRISE_TYPE = 4,
                            City = "Santiago",
                            Country = "Chile",
                            Description = "Capta Hydro is a clean energy technology startup that develops and commercialises hydropower technology for generation in artificial canals without the need of falls, that are economically competitive to other distributed generation alternatives and electric utility tariffs. Our BHAG is to install 1 GW of our technology  by 2027.",
                            Name = "Capta Hydro",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 3,
                            CD_ENTERPRISE_TYPE = 8,
                            City = "Santiago",
                            Country = "Chile",
                            Description = "Our product, Ceptinel Risk Manager, is a real-time business procecess monitoring system that, with the use of applied business rules algorithms, detects and notifies when potentially harmful or beneficial situations are detected.",
                            Name = "Ceptinel",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 4,
                            CD_ENTERPRISE_TYPE = 4,
                            City = "ProvIdencia",
                            Country = "Chile",
                            Description = "The business of changing the world into a green one is not an easy task. eGreen believes that by making things easy for users, the growth and impact of an environmentally friendly society can be greatly accelerated. eGreen's technology allows users to both calculate and offset their carbon footprint simply and free.  ",
                            Name = "CO2fine spa (egreen.green)",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 5,
                            CD_ENTERPRISE_TYPE = 8,
                            City = "Santiago",
                            Country = "Chile",
                            Description = "Coinaction provIdes worldwIde cheaper currency exchange. ",
                            Name = "Coinaction",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 6,
                            CD_ENTERPRISE_TYPE = 8,
                            City = "Santiago",
                            Country = "Chile",
                            Description = "CryptoMarket is a Chilean Fintech product created by both an engineer and a lawyer.    It is based on trust and development on Blockchain technology; CryptoMarket permits a new way of sending and receiving inmediatamente Value, simple, trustworthy and safe,  integrating Chilean tradicional banking systems to the Blockchain. ",
                            Name = "CryptoMkt",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 7,
                            CD_ENTERPRISE_TYPE = 3,
                            City = "Santiago",
                            Country = "Chile",
                            Description = "The RapId  Antibiotic Sensitivty Kit  developed by Diagnochip allows  people having a urinary tract infection to receive the results of the proper antibiotic to use in only 8 hours versus the 3 to 4 days required by the traditional methodology.  Our kit has a big social impact as close the gap in the access to medical care for vulnerable people living in developing countries  and rural areas merging accuraCity of the results with cost - efficiency.",
                            Name = "Diagnochip SpA",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 8,
                            CD_ENTERPRISE_TYPE = 9,
                            City = "Villa alemana",
                            Country = "Chile",
                            Description = "We are a R&D company , We are asociate with big manufacturer Company to test technology and domestic market acceptance . We are giving entrepreneurs services to acomplish goals in getting new products under quality certification and HACCP standards .We can give the scientific research to new food products under healthy and nutritional standards caring enviroment (sostain and recycling ), social and human rights and fairtrade standards . Our goal is having a Ideas trade tank giving them chances and yield to allow any people invest in them t support their success.",
                            Name = "frutnutspa",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 9,
                            CD_ENTERPRISE_TYPE = 9,
                            City = "Las Condes",
                            Country = "Chile",
                            Description = "green equitable is a community (franchise) of  restaurants, stores and markets where regular  and conscious people can choose top healthy,  local, organic and fair trade products while  preserving the environment. ",
                            Email = "",
                            Facebook = "",
                            Linkedin = "",
                            Name = "Green Equitable",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            Phone = "",
                            Photo = "/uploads/enterprise/Photo/22/diversas-imagens-fofas-50.jpg",
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Twitter = "",
                            Value = 0m
                        },
                        new
                        {
                            Id = 10,
                            CD_ENTERPRISE_TYPE = 6,
                            City = "Santiago",
                            Country = "Chile",
                            Description = @"HoliPlay Games is proud to present HoliMaths X, award winning family math games. A must have tool for families that understand how effective learning through playing can be.   Hi! I'm Matt from Holiplay Games!  HoliMaths X - a new family math strategy card game  We have an award winning game with 20+ outstanding reviews done by the most prominent Homeschoolers from the IHSNET network and other Parenting bloggers, communities and websites. It is the winner of the Academics' Choice Awards 2016 Winner! Brain Toy category. 
ehind due to all the technology that surrounds us.
",
                            Name = "HoliPlay Games",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 11,
                            CD_ENTERPRISE_TYPE = 5,
                            City = "Santiago",
                            Country = "Chile",
                            Description = "We believe that the world would be a better place to live in, if more companies were purpose-driven companies. There is a global trend of purpose-driven companies, aiming to solve a social and/or environmental challenges through their business. There�s also a growing number of consumers declaring to be �conscious� and wanting to know what they are consuming and which are the companies behind the products and services they prefer. Finally, these consumers are using more and more e-commerce to buy their products and services. The problem is that purpose-driven companies and conscious consumers don�t have a common place to find each other. BirusMarket.com is the online platform that allows conscious consumers, not only to find and get to know these purpose-driven companies, but also to buy their products and services. BirusMarket.com business model consIders incomes from sales fee.",
                            Name = "MercadoBirus.com",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 12,
                            CD_ENTERPRISE_TYPE = 2,
                            City = "Santiago",
                            Country = "Chile",
                            Description = "NexAtlas develops a \"Google Maps for aviation\" application for private and business aviation pilots. It consIderably facilitates access and handling of the most important flight planning and air navigation related information.",
                            Name = "NexAtlas",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        },
                        new
                        {
                            Id = 13,
                            CD_ENTERPRISE_TYPE = 5,
                            City = "Santiago",
                            Country = "Chile",
                            Description = "Impresee allows your e-commerce customers to find the products they want, whenever they want, thanks to its revolutionary search technology that uses Photos, sketches and text. Finding products with Impresee is easy and fun, whether it is a carpet seen at a friend's house or a special lamp they found really attractive. The cloud based API offered by Impresee allows any retail business to use this advanced search technology in its on-line stores, along with a data analysis system for a better understanding of the behavior and preferences of its customers. ",
                            Email = "",
                            Facebook = "",
                            Linkedin = "",
                            Name = "Orand / Impresee",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            Phone = "",
                            Photo = "/uploads/enterprise/Photo/34/americamg.png",
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Twitter = "",
                            Value = 0m
                        },
                        new
                        {
                            Id = 14,
                            CD_ENTERPRISE_TYPE = 8,
                            City = "Las Condes",
                            Country = "Chile",
                            Description = "PARADIGMA Ltda. has committed itself to deliver complete Solutions for agents and final consumers with the desire to promote the viability of the Financial Inclusion and building of assets among low-income indivIduals, small firms and their families on a large scale with Mobile devices, Computers, the Internet cloud, and the trust that brings the emergent technology Blockchain.",
                            Name = "Paradigma Ltda",
                            OwnEnterprise = false,
                            OwnShares = 0,
                            SharePrice = 5000.0m,
                            Shares = 0,
                            Value = 0m
                        });
                });

            modelBuilder.Entity("Ioasys.Data.Entities.EnterpriseType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CD_ENTERPRISE_TYPE")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("NM_ENTERPRISE_TYPE")
                        .HasColumnType("VARCHAR(50)");

                    b.HasKey("Id");

                    b.ToTable("ENTERPRISE_TYPES");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Name = "Agro"
                        },
                        new
                        {
                            Id = 2,
                            Name = "Aviation"
                        },
                        new
                        {
                            Id = 3,
                            Name = "Biotech"
                        },
                        new
                        {
                            Id = 4,
                            Name = "Eco"
                        },
                        new
                        {
                            Id = 5,
                            Name = "Ecommerce"
                        },
                        new
                        {
                            Id = 6,
                            Name = "Education"
                        },
                        new
                        {
                            Id = 7,
                            Name = "Fashion"
                        },
                        new
                        {
                            Id = 8,
                            Name = "Fintech"
                        },
                        new
                        {
                            Id = 9,
                            Name = "Food"
                        },
                        new
                        {
                            Id = 10,
                            Name = "Games"
                        });
                });

            modelBuilder.Entity("Ioasys.Data.Entities.Investor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CD_INVESTOR")
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<decimal>("Balance")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("VL_BALANCE")
                        .HasColumnType("NUMERIC(15,2)")
                        .HasDefaultValue(0m);

                    b.Property<string>("City")
                        .IsRequired()
                        .HasColumnName("NM_CITY")
                        .HasColumnType("VARCHAR(50)");

                    b.Property<string>("Country")
                        .IsRequired()
                        .HasColumnName("NM_COUNTRY")
                        .HasColumnType("VARCHAR(50)");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnName("DS_EMAIL")
                        .HasColumnType("VARCHAR(100)");

                    b.Property<bool>("FirstAccess")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("FL_FIRST_ACCESS")
                        .HasColumnType("bit")
                        .HasDefaultValue(true);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("NM_INVESTOR")
                        .HasColumnType("VARCHAR(50)");

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasColumnName("DS_PASSWORD")
                        .HasColumnType("varchar(32)");

                    b.Property<string>("Photo")
                        .IsRequired()
                        .HasColumnName("URL_PHOTO")
                        .HasColumnType("VARCHAR(200)");

                    b.Property<decimal>("PortfolioValue")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("VL_PORTFOLIO")
                        .HasColumnType("NUMERIC(15,2)")
                        .HasDefaultValue(0m);

                    b.Property<bool>("SuperAngel")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("FL_SUPER_ANGEL")
                        .HasColumnType("bit")
                        .HasDefaultValue(false);

                    b.HasKey("Id");

                    b.ToTable("INVESTORS");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Balance = 350000.0m,
                            City = "BH",
                            Country = "Brasil",
                            Email = "testeapple@ioasys.com.br",
                            FirstAccess = true,
                            Name = "Teste Apple",
                            Password = "ed2b1f468c5f915f3f1cf75d7068baae",
                            Photo = "/uploads/investor/photo/1/cropped4991818370070749122.jpg",
                            PortfolioValue = 350000.0m,
                            SuperAngel = false
                        },
                        new
                        {
                            Id = 2,
                            Balance = 200000.0m,
                            City = "SP",
                            Country = "Brasil",
                            Email = "testemicrosoft@ioasys.com.br",
                            FirstAccess = true,
                            Name = "Teste Microsoft",
                            Password = "ed2b1f468c5f915f3f1cf75d7068baae",
                            Photo = "/uploads/investor/photo/1/cropped4991818370070749122.jpg",
                            PortfolioValue = 200000.0m,
                            SuperAngel = false
                        });
                });

            modelBuilder.Entity("Ioasys.Data.Entities.Portfolio", b =>
                {
                    b.Property<int>("IdEnterprise")
                        .HasColumnType("int");

                    b.Property<int>("IdInvestor")
                        .HasColumnType("int");

                    b.HasKey("IdEnterprise", "IdInvestor");

                    b.HasIndex("IdInvestor");

                    b.ToTable("PORTFOLIOS");

                    b.HasData(
                        new
                        {
                            IdEnterprise = 1,
                            IdInvestor = 1
                        },
                        new
                        {
                            IdEnterprise = 2,
                            IdInvestor = 1
                        },
                        new
                        {
                            IdEnterprise = 4,
                            IdInvestor = 1
                        });
                });

            modelBuilder.Entity("Ioasys.Data.Entities.Enterprise", b =>
                {
                    b.HasOne("Ioasys.Data.Entities.EnterpriseType", "EnterpriseType")
                        .WithMany("Enterprises")
                        .HasForeignKey("CD_ENTERPRISE_TYPE")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();
                });

            modelBuilder.Entity("Ioasys.Data.Entities.Portfolio", b =>
                {
                    b.HasOne("Ioasys.Data.Entities.Enterprise", "Enterprise")
                        .WithMany("Portfolios")
                        .HasForeignKey("IdEnterprise")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Ioasys.Data.Entities.Investor", "Investor")
                        .WithMany("Portfolios")
                        .HasForeignKey("IdInvestor")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
