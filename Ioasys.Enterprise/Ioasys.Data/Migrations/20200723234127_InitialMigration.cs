﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ioasys.Data.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ENTERPRISE_TYPES",
                columns: table => new
                {
                    CD_ENTERPRISE_TYPE = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NM_ENTERPRISE_TYPE = table.Column<string>(type: "VARCHAR(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ENTERPRISE_TYPES", x => x.CD_ENTERPRISE_TYPE);
                });

            migrationBuilder.CreateTable(
                name: "INVESTORS",
                columns: table => new
                {
                    CD_INVESTOR = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NM_INVESTOR = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    DS_EMAIL = table.Column<string>(type: "VARCHAR(100)", nullable: false),
                    DS_PASSWORD = table.Column<string>(type: "varchar(32)", nullable: false),
                    NM_CITY = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    NM_COUNTRY = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    VL_BALANCE = table.Column<decimal>(type: "NUMERIC(15,2)", nullable: false, defaultValue: 0m),
                    URL_PHOTO = table.Column<string>(type: "VARCHAR(200)", nullable: false),
                    VL_PORTFOLIO = table.Column<decimal>(type: "NUMERIC(15,2)", nullable: false, defaultValue: 0m),
                    FL_FIRST_ACCESS = table.Column<bool>(nullable: false, defaultValue: true),
                    FL_SUPER_ANGEL = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_INVESTORS", x => x.CD_INVESTOR);
                });

            migrationBuilder.CreateTable(
                name: "ENTERPRISES",
                columns: table => new
                {
                    CD_ENTERPRISE = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NM_ENTERPRISE = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    DS_ENTERPRISE = table.Column<string>(type: "VARCHAR(1000)", nullable: true),
                    DS_EMAIL = table.Column<string>(type: "VARCHAR(100)", nullable: true),
                    URL_FACEBOOK = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    URL_TWITTER = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    URL_LINKEDIN = table.Column<string>(type: "VARCHAR(200)", nullable: true),
                    DS_PHONE = table.Column<string>(type: "VARCHAR(14)", nullable: true),
                    FL_OWN_ENTERPRISE = table.Column<bool>(nullable: false),
                    URL_PHOTO = table.Column<string>(type: "VARCHAR(300)", nullable: true),
                    VL_ENTERPRISE = table.Column<decimal>(nullable: false),
                    QT_SHARES = table.Column<int>(nullable: false),
                    VL_SHARE_PRICE = table.Column<decimal>(nullable: false),
                    QT_OWN_SHARES = table.Column<int>(nullable: false),
                    NM_CITY = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    NM_COUNTRY = table.Column<string>(type: "VARCHAR(50)", nullable: false),
                    CD_ENTERPRISE_TYPE = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ENTERPRISES", x => x.CD_ENTERPRISE);
                    table.ForeignKey(
                        name: "FK_ENTERPRISES_ENTERPRISE_TYPES_CD_ENTERPRISE_TYPE",
                        column: x => x.CD_ENTERPRISE_TYPE,
                        principalTable: "ENTERPRISE_TYPES",
                        principalColumn: "CD_ENTERPRISE_TYPE",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PORTFOLIOS",
                columns: table => new
                {
                    IdInvestor = table.Column<int>(nullable: false),
                    IdEnterprise = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PORTFOLIOS", x => new { x.IdEnterprise, x.IdInvestor });
                    table.ForeignKey(
                        name: "FK_PORTFOLIOS_ENTERPRISES_IdEnterprise",
                        column: x => x.IdEnterprise,
                        principalTable: "ENTERPRISES",
                        principalColumn: "CD_ENTERPRISE",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PORTFOLIOS_INVESTORS_IdInvestor",
                        column: x => x.IdInvestor,
                        principalTable: "INVESTORS",
                        principalColumn: "CD_INVESTOR",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "ENTERPRISE_TYPES",
                columns: new[] { "CD_ENTERPRISE_TYPE", "NM_ENTERPRISE_TYPE" },
                values: new object[,]
                {
                    { 1, "Agro" },
                    { 2, "Aviation" },
                    { 3, "Biotech" },
                    { 4, "Eco" },
                    { 5, "Ecommerce" },
                    { 6, "Education" },
                    { 7, "Fashion" },
                    { 8, "Fintech" },
                    { 9, "Food" },
                    { 10, "Games" }
                });

            migrationBuilder.InsertData(
                table: "INVESTORS",
                columns: new[] { "CD_INVESTOR", "VL_BALANCE", "NM_CITY", "NM_COUNTRY", "DS_EMAIL", "FL_FIRST_ACCESS", "NM_INVESTOR", "DS_PASSWORD", "URL_PHOTO", "VL_PORTFOLIO" },
                values: new object[,]
                {
                    { 1, 350000.0m, "BH", "Brasil", "testeapple@ioasys.com.br", true, "Teste Apple", "ed2b1f468c5f915f3f1cf75d7068baae", "/uploads/investor/photo/1/cropped4991818370070749122.jpg", 350000.0m },
                    { 2, 200000.0m, "SP", "Brasil", "testemicrosoft@ioasys.com.br", true, "Teste Microsoft", "ed2b1f468c5f915f3f1cf75d7068baae", "/uploads/investor/photo/1/cropped4991818370070749122.jpg", 200000.0m }
                });

            migrationBuilder.InsertData(
                table: "ENTERPRISES",
                columns: new[] { "CD_ENTERPRISE", "CD_ENTERPRISE_TYPE", "NM_CITY", "NM_COUNTRY", "DS_ENTERPRISE", "DS_EMAIL", "URL_FACEBOOK", "URL_LINKEDIN", "NM_ENTERPRISE", "FL_OWN_ENTERPRISE", "QT_OWN_SHARES", "DS_PHONE", "URL_PHOTO", "VL_SHARE_PRICE", "QT_SHARES", "URL_TWITTER", "VL_ENTERPRISE" },
                values: new object[,]
                {
                    { 1, 1, "Maule", "Chile", "Cold Killer was discovered by chance in the �90 s and developed by Mrs. In�s Artozon Sylvester while she was 70 years old. Ending in a U.S. patent granted and a new company, AQM S.A. Diluted in water and applied to any vegetable leaves, stimulate increase glucose (sugar) up to 30% therefore help plants resists cold weather. ", null, null, null, "AQM S.A.", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 12, 2, "Santiago", "Chile", "NexAtlas develops a \"Google Maps for aviation\" application for private and business aviation pilots. It consIderably facilitates access and handling of the most important flight planning and air navigation related information.", null, null, null, "NexAtlas", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 7, 3, "Santiago", "Chile", "The RapId  Antibiotic Sensitivty Kit  developed by Diagnochip allows  people having a urinary tract infection to receive the results of the proper antibiotic to use in only 8 hours versus the 3 to 4 days required by the traditional methodology.  Our kit has a big social impact as close the gap in the access to medical care for vulnerable people living in developing countries  and rural areas merging accuraCity of the results with cost - efficiency.", null, null, null, "Diagnochip SpA", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 2, 4, "Santiago", "Chile", "Capta Hydro is a clean energy technology startup that develops and commercialises hydropower technology for generation in artificial canals without the need of falls, that are economically competitive to other distributed generation alternatives and electric utility tariffs. Our BHAG is to install 1 GW of our technology  by 2027.", null, null, null, "Capta Hydro", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 4, 4, "ProvIdencia", "Chile", "The business of changing the world into a green one is not an easy task. eGreen believes that by making things easy for users, the growth and impact of an environmentally friendly society can be greatly accelerated. eGreen's technology allows users to both calculate and offset their carbon footprint simply and free.  ", null, null, null, "CO2fine spa (egreen.green)", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 11, 5, "Santiago", "Chile", "We believe that the world would be a better place to live in, if more companies were purpose-driven companies. There is a global trend of purpose-driven companies, aiming to solve a social and/or environmental challenges through their business. There�s also a growing number of consumers declaring to be �conscious� and wanting to know what they are consuming and which are the companies behind the products and services they prefer. Finally, these consumers are using more and more e-commerce to buy their products and services. The problem is that purpose-driven companies and conscious consumers don�t have a common place to find each other. BirusMarket.com is the online platform that allows conscious consumers, not only to find and get to know these purpose-driven companies, but also to buy their products and services. BirusMarket.com business model consIders incomes from sales fee.", null, null, null, "MercadoBirus.com", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 13, 5, "Santiago", "Chile", "Impresee allows your e-commerce customers to find the products they want, whenever they want, thanks to its revolutionary search technology that uses Photos, sketches and text. Finding products with Impresee is easy and fun, whether it is a carpet seen at a friend's house or a special lamp they found really attractive. The cloud based API offered by Impresee allows any retail business to use this advanced search technology in its on-line stores, along with a data analysis system for a better understanding of the behavior and preferences of its customers. ", "", "", "", "Orand / Impresee", false, 0, "", "/uploads/enterprise/Photo/34/americamg.png", 5000.0m, 0, "", 0m },
                    { 10, 6, "Santiago", "Chile", @"HoliPlay Games is proud to present HoliMaths X, award winning family math games. A must have tool for families that understand how effective learning through playing can be.   Hi! I'm Matt from Holiplay Games!  HoliMaths X - a new family math strategy card game  We have an award winning game with 20+ outstanding reviews done by the most prominent Homeschoolers from the IHSNET network and other Parenting bloggers, communities and websites. It is the winner of the Academics' Choice Awards 2016 Winner! Brain Toy category. 
                ehind due to all the technology that surrounds us.
                ", null, null, null, "HoliPlay Games", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 3, 8, "Santiago", "Chile", "Our product, Ceptinel Risk Manager, is a real-time business procecess monitoring system that, with the use of applied business rules algorithms, detects and notifies when potentially harmful or beneficial situations are detected.", null, null, null, "Ceptinel", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 5, 8, "Santiago", "Chile", "Coinaction provIdes worldwIde cheaper currency exchange. ", null, null, null, "Coinaction", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 6, 8, "Santiago", "Chile", "CryptoMarket is a Chilean Fintech product created by both an engineer and a lawyer.    It is based on trust and development on Blockchain technology; CryptoMarket permits a new way of sending and receiving inmediatamente Value, simple, trustworthy and safe,  integrating Chilean tradicional banking systems to the Blockchain. ", null, null, null, "CryptoMkt", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 14, 8, "Las Condes", "Chile", "PARADIGMA Ltda. has committed itself to deliver complete Solutions for agents and final consumers with the desire to promote the viability of the Financial Inclusion and building of assets among low-income indivIduals, small firms and their families on a large scale with Mobile devices, Computers, the Internet cloud, and the trust that brings the emergent technology Blockchain.", null, null, null, "Paradigma Ltda", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 8, 9, "Villa alemana", "Chile", "We are a R&D company , We are asociate with big manufacturer Company to test technology and domestic market acceptance . We are giving entrepreneurs services to acomplish goals in getting new products under quality certification and HACCP standards .We can give the scientific research to new food products under healthy and nutritional standards caring enviroment (sostain and recycling ), social and human rights and fairtrade standards . Our goal is having a Ideas trade tank giving them chances and yield to allow any people invest in them t support their success.", null, null, null, "frutnutspa", false, 0, null, null, 5000.0m, 0, null, 0m },
                    { 9, 9, "Las Condes", "Chile", "green equitable is a community (franchise) of  restaurants, stores and markets where regular  and conscious people can choose top healthy,  local, organic and fair trade products while  preserving the environment. ", "", "", "", "Green Equitable", false, 0, "", "/uploads/enterprise/Photo/22/diversas-imagens-fofas-50.jpg", 5000.0m, 0, "", 0m }
                });

            migrationBuilder.InsertData(
                table: "PORTFOLIOS",
                columns: new[] { "IdEnterprise", "IdInvestor" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "PORTFOLIOS",
                columns: new[] { "IdEnterprise", "IdInvestor" },
                values: new object[] { 2, 1 });

            migrationBuilder.InsertData(
                table: "PORTFOLIOS",
                columns: new[] { "IdEnterprise", "IdInvestor" },
                values: new object[] { 4, 1 });

            migrationBuilder.CreateIndex(
                name: "IX_ENTERPRISES_CD_ENTERPRISE_TYPE",
                table: "ENTERPRISES",
                column: "CD_ENTERPRISE_TYPE");

            migrationBuilder.CreateIndex(
                name: "IX_PORTFOLIOS_IdInvestor",
                table: "PORTFOLIOS",
                column: "IdInvestor");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PORTFOLIOS");

            migrationBuilder.DropTable(
                name: "ENTERPRISES");

            migrationBuilder.DropTable(
                name: "INVESTORS");

            migrationBuilder.DropTable(
                name: "ENTERPRISE_TYPES");
        }
    }
}
