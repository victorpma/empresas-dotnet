﻿using System.Collections.Generic;

namespace Ioasys.Data.Entities
{
    public class EnterpriseType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Enterprise> Enterprises { get; set; }
    }
}
