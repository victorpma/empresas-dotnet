﻿using System.Collections.Generic;
using System.Linq;

namespace Ioasys.Data.Entities
{
    public class Investor
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public decimal Balance { get; set; }

        public string Photo { get; set; }

        public virtual ICollection<Portfolio> Portfolios { get; set; } = new List<Portfolio>();

        public virtual List<Enterprise> Enterprises { get; set; }

        public decimal PortfolioValue { get; set; }

        public bool FirstAccess { get; set; }

        public bool SuperAngel { get; set; }
    }
}
