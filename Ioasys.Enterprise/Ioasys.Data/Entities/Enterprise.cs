﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Ioasys.Data.Entities
{
    public class Enterprise
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Email { get; set; }

        public string Facebook { get; set; }

        public string Twitter { get; set; }

        public string Linkedin { get; set; }

        public string Phone { get; set; }

        public bool OwnEnterprise { get; set; }

        public string Photo { get; set; }

        public decimal Value { get; set; }

        public int Shares { get; set; }

        public decimal SharePrice { get; set; }

        public int OwnShares { get; set; }

        public string City { get; set; }

        public string Country { get; set; }
        
        public int CD_ENTERPRISE_TYPE { get; set; } //IGNORE ESTE CAMPO, FOI APENAS PARA INSERIR SEED DA TABELA PARA TESTES DA PROVA, E INSERIR A RELAÇÃO DA FOREIGN KEY VIA JSON

        public virtual EnterpriseType EnterpriseType { get; set; }

        public virtual ICollection<Portfolio> Portfolios { get; set; } = new List<Portfolio>();

        public virtual List<Investor> Investors => Portfolios.Select(p => p.Investor).ToList();

    }
}
