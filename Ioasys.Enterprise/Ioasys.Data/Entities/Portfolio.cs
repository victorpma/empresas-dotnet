﻿namespace Ioasys.Data.Entities
{
    public class Portfolio
    {
        public int IdInvestor { get; set; }
        public int IdEnterprise { get; set; }

        public virtual Investor Investor { get; set; }
        public virtual Enterprise Enterprise { get; set; }
    }
}
