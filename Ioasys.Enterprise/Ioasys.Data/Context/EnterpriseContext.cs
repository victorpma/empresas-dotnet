﻿using Ioasys.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Ioasys.Data.Context
{
    public class EnterpriseContext : DbContext
    {
        public EnterpriseContext(DbContextOptions<EnterpriseContext> options) : base(options) { }

        public DbSet<Investor> Investors { get; set; }
        public DbSet<EnterpriseType> EnterpriseTypes { get; set; }
        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EnterpriseContext).Assembly);

            base.OnModelCreating(modelBuilder);
        }
    }
}
